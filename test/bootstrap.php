<?php
/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - fongo.dev
 * ========================================================================== */

$root_dir = realpath(__DIR__ . '/..');

require $root_dir . '/src/initme.php';

define('SATAO_TEST_DIR', $root_dir . '/test');

const SATAO_TEST_DATA_DIR = SATAO_TEST_DIR . '/data';
const SATAO_TEST_FUNC_DIR = SATAO_TEST_DIR . '/func';
