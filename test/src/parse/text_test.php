<?php

/* =============================================================================
 * Satao - Copyright (c) Andrea Davanzo - License MPL v2.0 - adavanzo.com
 * ========================================================================== */

declare(strict_types=1);

use bateo_test as test;

class bateo_testcase implements bateo_testcase_interface
{

  public function setup()
  {
    require_once SATAO_DIR . '/parse/text.php';
  }

  public function teardown()
  {

  }

  public function test_basename(test $t, string $basename, array $ext_macros = []): string
  {
    $diff_string = '';
    $t->wie = file_get_contents(SATAO_TEST_DATA_DIR . '/' . $basename . '.html');
    try{
      $t->wig = satao_parse_text(file_get_contents(SATAO_TEST_DATA_DIR . '/' . $basename . '.txt'), $ext_macros);
    } catch (throwable $th) {

    }
    if ($t->wie !== $t->wig) {
      $diff_string .= "-- $basename --\n";
      $diff_string .= "Wie: " . str_replace("\n", "NEW_LINE", $t->wie);
      $diff_string .= "\n";
      $diff_string .= "Wig: " . str_replace("\n", "NEW_LINE", ($t->wig ?? ''));
    }
    if (isset($th)) {
      bateo_d($th->getmessage(), 'throwable');
    }
    return $diff_string;
  }

  public function t_simple_line(test $t)
  {
    $diff_string = $this->test_basename($t, 'simple_line');
    $t->pass_if($t->wie === $t->wig, $diff_string);
  }

  public function t_paragraphs(test $t)
  {
    $diff_string = $this->test_basename($t, 'paragraphs');
    $t->pass_if($t->wie === $t->wig, $diff_string);
  }

  public function t_inline(test $t)
  {
    $diff_string = $this->test_basename($t, 'inline');
    $t->pass_if($t->wie === $t->wig, $diff_string);
  }

  public function t_attribs(test $t)
  {
    $diff_string = $this->test_basename($t, 'attribs');
    $t->pass_if($t->wie === $t->wig, $diff_string);
  }

  public function t_attribs_complex(test $t)
  {
    $diff_string = $this->test_basename($t, 'attribs_complex');
    $t->pass_if($t->wie === $t->wig, $diff_string);
  }

  public function t_void_element(test $t)
  {
    $diff_string = $this->test_basename($t, 'void_element');
    $t->pass_if($t->wie === $t->wig, $diff_string);
  }

  public function t_close_concatenation(test $t)
  {
    $diff_string = $this->test_basename($t, 'close_concatenation');
    $t->pass_if($t->wie === $t->wig, $diff_string);
  }


  public function t_triple_dots(test $t)
  {
    $diff_string = $this->test_basename($t, 'triple_dots');
    $t->pass_if($t->wie === $t->wig, $diff_string);
  }

  public function t_pre_code_with_newline(test $t)
  {
    $diff_string = $this->test_basename($t, 'pre_code_with_newline');
    $t->pass_if($t->wie === $t->wig, $diff_string);
  }


  public function t_tag_prev_char(test $t)
  {
    $diff_string = $this->test_basename($t, 'tag_prev_char');
    $t->pass_if($t->wie === $t->wig, $diff_string);
  }

  public function t_macro(test $t)
  {
    $diff_string = $this->test_basename($t, 'macro');
    $t->pass_if($t->wie === $t->wig, $diff_string);
  }

  public function t_external_macro(test $t)
  {
    $diff_string = $this->test_basename(
      $t,
      'external_macro',
      [
        'test-mymacro' => [
          'open' => '<pre class="test-mymacro">',
          'close' => '</pre>'
        ]
      ]
    );
    $t->pass_if($t->wie === $t->wig, $diff_string);
  }

  public function t_dot_something(test $t)
  {
    $diff_string = $this->test_basename($t, 'dot_something');
    $t->pass_if($t->wie === $t->wig, $diff_string);
  }

  public function t_ul_li_ul(test $t)
  {
    $diff_string = $this->test_basename($t, 'ul_li_ul');
    $t->pass_if($t->wie === $t->wig, $diff_string);
  }

  public function t_token_close_null(test $t)
  {
    $diff_string = $this->test_basename($t, 'token_close_null');
    $t->pass_if($t->wie === $t->wig, $diff_string);
  }

  public function t_valid_tag(test $t)
  {
    // 4.0) on the text is not a valid tag
    $diff_string = $this->test_basename($t, 'valid_tag');
    $t->pass_if($t->wie === $t->wig, $diff_string);
  }

  public function t_start_char_escaped(test $t)
  {
    $diff_string = $this->test_basename($t, 'start_char_escaped');
    $t->pass_if($t->wie === $t->wig, $diff_string);
  }

  public function t_escaped(test $t)
  {
    $diff_string = $this->test_basename($t, 'escaped');
    $t->pass_if($t->wie === $t->wig, $diff_string);
  }

  public function t_encoding(test $t)
  {
    $diff_string = $this->test_basename($t, 'encoding');
    $t->pass_if($t->wie === $t->wig, $diff_string);
  }

  public function t_encoding2(test $t)
  {
    $diff_string = $this->test_basename($t, 'encoding2');
    $t->pass_if($t->wie === $t->wig, $diff_string);
  }
}
