# satao

Satao is a document format for publishing HTML documents.
Based and inspired by HTML, it reuses the HTML elements trying to reduce its syntax verbosity.
HTML has proven to be a strong and reliable document format, however the syntax is quite verbose.

For such reason, a lot of alternative markup languages has been created during this time: Markup, Asciidoc or reStructuredText etc. which have their own syntax and rules.
Satao adopted a different approach. Instead to invent a new markup, it reuses the HTML tags and shortening its syntax.
So an HTML like this

```
<p>
  This is a sentence inside a paragraph.
  This is another sentence inside a paragraph.
</p>

<ul>
  <li>this is an item</li>
  <li>this is another item</li>
  <li>another one
    <ul>
      <li>this is a text</li>
      <li>this is another text</li>
    </ul>
  </li>
</ul>
```
In Satao can be written like this

```
.p
  This is a sentence inside a paragraph.
  This is another sentence inside a paragraph.

.ul
  .li this is an item<
  .li this is another item<
  .li
    .ol
      .li this is a text<
      .li this is another text<
```

Rules are quite simple. Please refer to the documnentation on Satao website https://satao.page

## How to use

Download the code

```php
require 'initme.php';
require_once SATAO_DIR . '/html.php';

$text = file_get_contents('yourfile.satao');
$html = satao_html($text);

```


## Contributing
Contributors are welcome.

## License
Satao is an open source project released under Mozilla Public License Version 2.0.
See LICENSE file or visit https://www.mozilla.org/en-US/MPL/2.0/
