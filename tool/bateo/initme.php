<?php

/* =============================================================================
 * Naranza Bateo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

const BATEO_DIR = __DIR__;
const BATEO_VERSION = '2024.3';
const BATEO_CODENAME = 'Araripe Manakin (Chiroxiphia bokermanni)';
const BATEO_TEST_PASS = 0;
const BATEO_TEST_FAIL = 1;
const BATEO_TEST_UNDEFINED = 2;
const BATEO_TEST_ERROR = 3;
const BATEO_TEST_HALT = 4;
const BATEO_TEST_SKIP = 5;
