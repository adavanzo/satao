<?php

/* =============================================================================
 * Naranza Bateo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

$config = [];

$config['recursive'] = true;
$config['testcase_summary_level'] = 1;
